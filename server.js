const express = require ('express');
const app     = express();
const port    = process.env.PORT || 8081;
const hbs     = require('hbs');
const bodyParser = require ('body-parser');
const livroRoutes = require('./routes/livro');

app.use(express.static(__dirname + '/public'));

//template engine - handlebars
app.set('view engine', 'hbs');

//Configuração das Paginas Parciais.
hbs.registerPartials(__dirname + '/views/partials');

// Rota de Livros
app.use('/livros', livroRoutes);

  
app.get('/',(req,res) => {
    // res.send ('epaaaa')
    res.render('index');
  
});

//configuralção do bodyParser

app.use(bodyParser.urlencoded({ extended: true }));


// app.get ('/livros', (req,res) =>{
//   res,send ('livrosssss');
// }); 
        

app.listen(port, () => {
  console.log(`The server is running in port ${port}.`);
});