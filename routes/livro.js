const express = require ('express');
const router = express.Router();

// Base de Dados Ficticia
const listaDeLivros = require ('./../seed/livros_seed');

router.get('/', (req, res) => {
  res.render('livros/index', { livros: listaDeLivros });
});

router.get('/novo', (req, res) => {
  res.render('livros/novo');
} );

router.post('/novo', (req, res) =>{
  res.send(req.body);
});
         

module.exports = router;
